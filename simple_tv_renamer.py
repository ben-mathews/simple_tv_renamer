import sys
import os
import re
import glob
import shutil
import argparse
import logging

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

def process_arguments():
    """
    Runs the script from the setuptools entry point
    """
    # Parse the command line
    argparser = argparse.ArgumentParser(
        description=
        (
            "Ben's simple tv show renamer.  Processes all files in a folder or subfolders, extracts out the SXXEYY "
            "string, and renames to SHOWNAME - SXXEYY.ext"
        ),
        add_help=False,  # Allows changing of help flag from -h to -H
    )
    argparser.add_argument(
        "--help",
        "-H",
        action="help",
        help="show this help message and exit",
    )
    argparser.add_argument(
        "--show_name",
        action="store",
        default='',
        help="source folder for renamed files",
    )
    argparser.add_argument(
        "--source_folder",
        action="store",
        default=os.getcwd(),
        help="source folder for renamed files",
    )
    argparser.add_argument(
        "--destination_folder",
        action="store",
        default=os.getcwd(),
        help="destination folder for renamed files",
    )
    argparser.add_argument(
        "--force",
        action='store_true',
        default=False,
        help="Use GUI directory selector instead of passing in arguments on command line"
    )
    argparser.add_argument(
        "--gui_select",
        action='store_true',
        default=False,
        help="Use GUI directory selector instead of passing in arguments on command line"
    )

    args = argparser.parse_args()

    if args.gui_select == True:
        read_last_config(args)
        gui_select_function(args)

    write_last_config(args)

    return args


def gui_select_function(args):
    """
    A helper function to allow the graphical selection of source directory, destination directory, and show name
    :param args: Namespace object with input arguments
    :return: void
    """
    import tkinter as tk
    from tkinter import filedialog, simpledialog
    application_window = tk.Tk()
    source_folder = filedialog.askdirectory(parent=application_window, initialdir=find_existing_directory_in_path(args.source_folder), title="Select a Source Folder:")
    destination_folder = filedialog.askdirectory(parent=application_window, initialdir=find_existing_directory_in_path(args.destination_folder), title="Select a Destination Folder:")
    if type(destination_folder) is str:
        show_name = simpledialog.askstring("Input", "Enter Show Name", parent=application_window, initialvalue=os.path.split(destination_folder)[-1])
    else:
        show_name = None
    application_window.quit()
    application_window.destroy()
    args.source_folder = source_folder
    args.destination_folder = destination_folder
    args.show_name = show_name


def find_existing_directory_in_path(path):
    """
    Ascends a path and stops when the current path exists on the file system.
    :param path: String representing the path to ascend
    :return: The path that has been verified to exist
    """
    path = os.path.normpath(path)
    folders = path.split(os.sep)
    verified_path = os.sep.join(folders)
    while (len(folders)) > 1 and (not os.path.isdir(verified_path)):
        folders = folders[0:-1]
        verified_path = os.sep.join(folders)
    if verified_path =='':
        verified_path = os.getcwd()
    return verified_path


def write_last_config(args):
    """
    Writes the last configuration to ~/.config/simple_file_renamer/config.ini
    :param args:
    :return:
    """
    import os, shutil, configparser

    user_config_dir = os.path.expanduser("~") + "/.config/simple_tv_renamer"
    user_config = user_config_dir + "/config.ini"

    if not os.path.isfile(user_config):
        os.makedirs(user_config_dir, exist_ok=True)

    config = configparser.ConfigParser()

    config.add_section('main')
    config['main']['source_folder'] = args.source_folder
    config['main']['destination_folder'] = args.destination_folder
    config['main']['show_name'] = args.show_name

    with open(user_config, 'w') as f:
        config.write(f)


def read_last_config(args):
    """
    Reads the configuration from ~/.config/simple_file_renamer/config.ini
    :param args:
    :return:
    """
    import os, configparser

    user_config_dir = os.path.expanduser("~") + "/.config/simple_tv_renamer"
    user_config = user_config_dir + "/config.ini"

    config = configparser.ConfigParser()
    config.read(user_config)

    args.source_folder = config['main']['source_folder']
    args.destination_folder = config['main']['destination_folder']
    args.show_name = config['main']['show_name']


def process_folder(source_folder, destination_folder, show_name, min_file_size_mb=100):
    """
    Processes a folder to move and rename files
    :param source_folder:
    :param destination_folder:
    :param show_name:
    :param min_file_size_mb:
    :return:
    """
    old_file_names = []
    new_file_names = []
    root_folder_contents = glob.glob(source_folder + '/*')
    for item in root_folder_contents:
        old_file_name = None
        path = os.path.normpath(item)
        folders = path.split(os.sep)
        m = re.search('(?i)S\d{1,2}E\d{1,2}', folders[-1])
        if m is None:
            logging.info('Season and Episode string not found for file {0}'.format(item))
            continue
        sxxexx = m.group(0)
        if os.path.isdir(item):
            subfolder_path = os.path.join(source_folder, folders[-1])
            episode_folder_contents = glob.glob(glob.escape(subfolder_path) + '/*')
            max_file_size = -1
            for file in episode_folder_contents:
                if os.path.isdir(file):
                    continue
                file_name = os.path.join(subfolder_path, file)
                file_size = os.path.getsize(file_name)
                if (file_size > max_file_size) and (file_size > min_file_size_mb * 1e6):
                    max_file_size = file_size
                    old_file_name = file_name
                    filename, file_extension = os.path.splitext(old_file_name)
        else:
            file_size = os.path.getsize(path)
            if file_size > min_file_size_mb * 1e6:
                old_file_name = path
                filename, file_extension = os.path.splitext(old_file_name)

        if old_file_name is not None:
            new_file_name = os.path.join(destination_folder,'{0} - {1}{2}'.format(show_name, str.upper(sxxexx), str.lower(file_extension)))
            old_file_names.append(old_file_name)
            new_file_names.append(new_file_name)

    for old_file_name, new_file_name in zip(old_file_names, new_file_names):
        print('Files to Rename:\n   Old Name: {0}\n   New Name: {1}'.format(old_file_name, new_file_name))

    if (args.force is False):
        answer = input("Proceed? (y/n)? ")
    else:
        answer = 'y'
    if answer == 'y':
        for old_file_name, new_file_name in zip(old_file_names, new_file_names):
            logging.info('Renaming:\n   Old Name: {0}\n   New Name: {1}'.format(old_file_name, new_file_name))
            shutil.move(old_file_name, new_file_name)
            logging.info('Done')


if __name__ == "__main__":
    args = process_arguments()
    assert os.path.isdir(args.source_folder), "Source folder does not exist"
    assert os.path.isdir(args.destination_folder), "Source folder does not exist"
    assert type(args.show_name) is str, "Show name must be a string"
    process_folder(source_folder=args.source_folder, destination_folder=args.destination_folder, show_name=args.show_name)
    logging.info('Done')

